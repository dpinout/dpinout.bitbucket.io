/**
 *
 * Emotion Tracking 
 */

let isDrawing = true;
let happyValue;

 function setup(){
   initVideo(640, 480, 1);
   initTracker();
   var cnv = createCanvas(640,480);
   cnv.parent('theCanvas');   
 }

 function draw(){
    showVideo();
   getEmotions();
   clear();

   // enable tracking
   if(isShowtracking){
     drawPoints();
   }

   // enable emotions
   if (emotions) {
      drawEmotionData();
      // Get value for a certain emotion - arraylist type
      //  andry=0, sad=1, surprised=2, happy=3
      happyValue = getEmotionData(3); // normalised values [0 - 1]
   }

   // enable drawing
   if(isDrawing) {
    noStroke();
    // mapping happiness value to colours
    let myCol = map(happyValue, 0.0, 1.0, 0, 255);
    fill(myCol, 0, 0);
    // mapping happiness value to x coordinate
    let x = map(happyValue, 0.0, 1.0, 0, width);
    // mapping happiness value to diameter
    let s = map(happyValue, 0.0, 1.0, 0, 500);
    ellipse(x, height/2, s, s);
 }

 }
