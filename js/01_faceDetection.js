/**
*
*/

const imageUpload = document.getElementById('myImage')
const MODEL_URL = '/models'
let canvas;

Promise.all([
    faceapi.nets.tinyFaceDetector.loadFromUri(MODEL_URL),
    faceapi.nets.faceLandmark68Net.loadFromUri(MODEL_URL),
    faceapi.nets.faceRecognitionNet.loadFromUri(MODEL_URL),
    faceapi.nets.faceExpressionNet.loadFromUri(MODEL_URL),
    faceapi.nets.ssdMobilenetv1.loadFromUri(MODEL_URL)
  ]).then(start)

  function setup(){
    canvas = createCanvas(imageUpload.width, imageUpload.height);
    noStroke();
    textSize(12);
  }

   async function start() {
    document.body.append('loaded')
    //console.log('started');
    const container = document.createElement('div')
    container.style.position = 'relative'
    document.body.append(container)
    let img;
    
    imageUpload.addEventListener('change', async () => {
        if(img) img.remove();
        if(canvas) canvas.remove();
        img = await faceapi.bufferToImage(imageUpload.files[0])
        container.append(img)

        canvas = faceapi.createCanvasFromMedia(img)
        container.append(canvas)

        const fullFaceDescriptions = await faceapi.detectAllFaces(img).withFaceLandmarks().withFaceExpressions()
        let numFaces = fullFaceDescriptions.length
        let v = document.getElementById('numberOfFaces').innerHTML = numFaces
        //v.value = numFaces;
        //let numFaces = document.body.append(fullFaceDescriptions.length)
        const displaySize = { width: img.width, height: img.height }
        faceapi.matchDimensions(canvas, displaySize)
        
        const detections = await faceapi.detectAllFaces(img).withFaceLandmarks().withFaceExpressions()
        const resizedDetections = faceapi.resizeResults(detections, displaySize)
        faceapi.draw.drawDetections(canvas, resizedDetections)
        faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
        faceapi.draw.drawFaceExpressions(canvas, resizedDetections)

        
    })
  }
