/**
 * Image Classifier
 * https://ml5js.org/reference/api-ImageClassifier/
 */

 let classifier;
 let theVideo;
 let label, prob;
 let isCapture;
 let button;

 function isClick() {
     theVideo = createCapture(VIDEO);
     theVideo.size(320,240); // resize video for performance ?
     theVideo.hide();
     // init classifier with video & models from MobileNet
     classifier = ml5.imageClassifier('Darknet', theVideo, modelReady);
     isCapture = !isCapture;
     if(isCapture === false){
       theVideo.stop();
       background(0);
     }
 }

 function setup(){
   let cnv = createCanvas(640,480);
   cnv.parent('theCanvas');
   background(0);
 }

 function draw(){
   if(isCapture){
     background(0);
     image(theVideo, 0, 0);
     fill(255);
     textSize(53);
     text(label, 20, 350);
     text(prob, 20, 400);
     textSize(23);
     text("Mouse click to classify image", 20, height-40);
   }
  }

function mousePressed() {
    runML();
  }

function runML() {
    classifier.predict(gotResults);
  }

function gotResults(err, results){
  if (err) {
    console.error(err);
  }else{
   //console.log('Result!');
   //console.log(results);
   label = results[0].label;
   prob = results[0].confidence;
 }
}

 function modelReady(){
   select('#status').elt.innerText = 'Model Loaded';
   runML();
 }

 