/**
 * Image Classifier
 * https://ml5js.org/reference/api-FeatureExtractor/
 * Example for saving data :
 * https://github.com/ml5js/ml5-examples/blob/master/p5js/FeatureExtractor/FeatureExtractor_Image_Classification/sketch.js
 */

let mNet;
let classifier;
let theVideo;
let label, prob;
let beerMatButton;
let postCard;
let init;
let train;
let classify;
let isVideo = true;

function setup() {
    let cnv = createCanvas(600, 460);
    cnv.parent('theCanvas');
    background(0);
    theVideo = createCapture(VIDEO);
    theVideo.size(320, 240); // resize video for performance ?
    theVideo.hide();

    mNet = ml5.featureExtractor('MobileNet', theVideo, modelReady);
    const options = { numLabels: 3 }; // important for adding various classes!
    classifier = mNet.classification(theVideo, options);
    
    init = createButton('Init');
    init.mousePressed(function () {
        classifier.addImage('Init');
    });
    beerMatButton = createButton('Beer Mat');
    beerMatButton.mousePressed(function () {
        classifier.addImage('Beer Mat');
    });

    postCard = createButton('Postcard');
    postCard.mousePressed(function () {
        classifier.addImage('Postcard');
    });

    train = createButton('TRAIN');
    train.mousePressed(function () {
        classifier.train(isTraining);
    });

}

function draw() {
    background(0);
    if (isVideo) {
        image(theVideo, 0, 0);
    }
    fill(255, 0, 0);
    textSize(58);
    if(label != 'Init'){
        text(label, 20, 90);
        text(prob, 20, 170);
    }
}


// Retrain the network
function isTraining(lossValue) {
    if (lossValue == null) {
        console.log('training finished');
        classifier.classify(gotResults);
    } else {
        console.log('Loss is', lossValue);
    }
};


function gotResults(err, results) {
    if (err) {
        console.error(err);
    } else {
        console.log('Result!');
        console.log(results);
        label = results[0].label;
        prob = results[0].confidence;
        classifier.classify(gotResults);

    }
}

function videoReady() {
    console.log("Video is ready");
}

function modelReady() {
    document.getElementById('status').innerHTML = 'model loaded';
}

function keyPressed(){
    if(key ==='v'){
        isVideo =!isVideo;
    }
}