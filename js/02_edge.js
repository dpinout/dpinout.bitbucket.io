/**
 * Source : 
 * https://kylemcdonald.github.io/cv-examples/
 *
 */


let capture;
let buffer;
let result;
let w = 640,
    h = 480;
let isNegative = false;    

function setup() {
    capture = createCapture({
        audio: false,
        video: {
            width: w,
            height: h
        }
    }, function() {
        console.log('capture ready.')
    });
    let cnv = createCanvas(w, h);
    cnv.parent('theCanvas');
    capture.size(w, h);
    capture.hide();
    buffer = new jsfeat.matrix_t(w, h, jsfeat.U8C1_t);
}

function jsfeatToP5(src, dst) {
    if (!dst || dst.width != src.cols || dst.height != src.rows) {
        dst = createImage(src.cols, src.rows);
    }
    var n = src.data.length;
    dst.loadPixels();
    var srcData = src.data;
    var dstData = dst.pixels;
    for (var i = 0, j = 0; i < n; i++) {
        var cur = srcData[i];
        dstData[j++] = cur;
        dstData[j++] = cur;
        dstData[j++] = cur;
        dstData[j++] = 255;
    }
    dst.updatePixels();
    return dst;
}

function draw() {
    image(capture, 0, 0, 640, 480);
    capture.loadPixels();
    if (capture.pixels.length > 0) { 
        var blurSize = select('#blurSize').elt.value;
        var lowThreshold = select('#lowThreshold').elt.value;
        var highThreshold = select('#highThreshold').elt.value;

        blurSize = map(blurSize, 0, 100, 1, 12);
        lowThreshold = map(lowThreshold, 0, 100, 0, 255);
        highThreshold = map(highThreshold, 0, 100, 0, 255);

        jsfeat.imgproc.grayscale(capture.pixels, w, h, buffer);
        jsfeat.imgproc.gaussian_blur(buffer, buffer, blurSize, 0);
        jsfeat.imgproc.canny(buffer, buffer, lowThreshold, highThreshold);
        var n = buffer.rows * buffer.cols;
        // uncomment the following lines to invert the image
        if(isNegative){
            for (var i = 0; i < n; i++) {
                buffer.data[i] = 255 - buffer.data[i];
           }
    }
        result = jsfeatToP5(buffer, result);
        image(result, 0, 0, 640, 480);

        // Values
        select('#blurVal').elt.innerText = int(blurSize);
        select('#lowThreshVal').elt.innerText = int(lowThreshold);
        select('#highThreshVal').elt.innerText = int(highThreshold);
    }
}

function keyPressed(){
    isNegative =!isNegative;
}
