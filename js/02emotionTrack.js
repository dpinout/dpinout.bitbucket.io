/**
 *
 */

 const video = document.getElementById('video');
 const MODEL_URL = '/models'
 let canvas;
 let xPos, yPos, w, h;
 let isFace = false;
 let isLandMarks = true;
 let angry, disgusted, fearful, happy, neutral, sad, surprised;

Promise.all([
    faceapi.nets.tinyFaceDetector.loadFromUri(MODEL_URL),
    faceapi.nets.faceLandmark68Net.loadFromUri(MODEL_URL),
    faceapi.nets.faceRecognitionNet.loadFromUri(MODEL_URL),
    faceapi.nets.faceExpressionNet.loadFromUri(MODEL_URL)
  ]).then(startVideo)


 function startVideo() {
    navigator.getUserMedia(
      { video: {} },
      stream => video.srcObject = stream,
      err => console.error(err)
    )
  }

  function setup(){
    canvas = createCanvas(video.width, video.height);
    noStroke();
    textSize(12);
  }

  function draw(){
      clear();
      //let myCol = map(happy, 0, 1, 0, 255);
      //fill(myCol, myCol, 0), 150;
      fill(0,0,255, 150);
      if(isFace) {
        rect(xPos+50,yPos+25,w,h);
        fill(0,0,255);
        
        text('HAPPY: '+happy, 50, 50);
      }
}

function keyPressed(){
    if(key === 'm'){
        isLandMarks = !isLandMarks;
    }
}

  video.addEventListener('play', () => {
    const canvas = faceapi.createCanvasFromMedia(video)
    document.body.append(canvas)
    const displaySize = { width: video.width, height: video.height }
    faceapi.matchDimensions(canvas, displaySize)
    setInterval(async () => {
      const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions()
      //console.log(detections);
      if(detections.length>0){
        isFace = true;
      }else{
        isFace = false;
      }
      if(isFace) {
       xPos = detections[0].detection._box._x;
       yPos = detections[0].detection._box._y;
       w = detections[0].detection._box._width;
       h = detections[0].detection._box._height;

      //console.log(detections[0].expressions);
      const expressions = detections[0].expressions;
       angry = detections[0].expressions.angry;
       disgusted = detections[0].expressions.disgusted;
       fearful = detections[0].expressions.fearful;
       happy = detections[0].expressions.happy;
       neutral = detections[0].expressions.neutral;
       sad = detections[0].expressions.sad;
       surprised = detections[0].expressions.surprised;
      //console.log(detections[0].expressions);
      }
      const resizedDetections = faceapi.resizeResults(detections, displaySize)
      canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
      faceapi.draw.drawDetections(canvas, resizedDetections)
      if(isLandMarks){
        faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
        //faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
      }
    }, 150)
  })
