/**
 *
 * Face Detection
 * Sketch Utils
 */

let ctracker;
let theVideo;
let positions = [];
let isShowVideo;
let isShowtracking = true;
let isEmotionData = true;
let vidScale;

/**
 * Emotion global variables
 */

delete emotionModel['disgusted'];
delete emotionModel['fear'];
var emotions = new emotionClassifier();
var predictedEmotions;
emotions.init(emotionModel);
var emotionData = emotions.getBlank();


// Eventual sketch special config
function config(v) {
  v;
}

function initVideo(_w, _h, _s) {
  vidScale = _s
  theVideo = createCapture(VIDEO);
  theVideo.size(_w/vidScale, _h/vidScale);
  //theVideo.id("theCanvas");
}

function initTracker() {
  ctracker = new clm.tracker({useWebGL : true});
  pModel.shapeModel.nonRegularizedVectors.push(9);
  pModel.shapeModel.nonRegularizedVectors.push(11);

  ctracker.init(pModel);
  ctracker.start(theVideo.elt);

}

/**
 * show video
 */

function showVideo() {
  if (isShowVideo) {
    theVideo.hide();
  } else {
    theVideo.show();
  }
}

/*
 * Gets & draws face tracking points
 */

function drawPoints() {
  positions = ctracker.getCurrentPosition();
  noStroke();
  fill(255, 0, 0);
  for (var i = 0; i < positions.length - 3; i++) {
    ellipse(positions[i][0], positions[i][1], 3, 3);
  }
}


/*
 * Loads parameters for emotion tracking
 */

function getEmotions() {
  var cp = ctracker.getCurrentParameters();
  predictedEmotions = emotions.meanPredict(cp);
}


/**
 * Simple viz of emotion data
 */
function drawEmotionData() {
  fill(0,0,255);
  if(isEmotionData) {
  // andry=0, sad=1, surprised=2, happy=3
  for (var i = 0; i < predictedEmotions.length; i++) {
    rect(i * 110 + 20, height - 80, 30, -predictedEmotions[i].value * 30);
  }

  text("ANGRY", 20, height - 40);
  text("SAD", 130, height - 40);
  text("SURPRISED", 220, height - 40);
  text("HAPPY", 340, height - 40);
  }
}
/**
 * Get emotion value as float by emotion type
 * andry=0, sad=1, surprised=2, happy=3
 */
function getEmotionData(_type){
  var theEmValue = 0;
  for (var i = 0; i < predictedEmotions.length; i++) {
    theEmValue = predictedEmotions[_type].value
  }
  return theEmValue;
}


////////////////////////// KEYS
function keyTyped() {
  if (key == 'v') {
    isShowVideo = !isShowVideo;
  }

  if (key == 't') {
    isShowtracking = !isShowtracking;
  }

  if (key == 'e') {
    isEmotionData = !isEmotionData;
  }

  if(key == 'd'){
    isDrawing =!isDrawing;
  }
}
