var wind;
var position;
var data;
var typo;
var lieu, degres, img;
    
function preload() {
  var url = 'https://api.apixu.com/v1/current.json?key=513d8003c8b348f1a2461629162106&q=LILLE';
  data = loadJSON(url);
  typo = loadFont('../media/OfficeCodePro-Medium.ttf');    
}

function setup() {
  createCanvas(innerWidth/2, innerHeight);
  textFont(typo);
  textSize(30);
  lieu = data.location.name;
  degres = data.current.temp_c;
  img = createImg(data.current.condition.icon);
  imageMode(CENTER);
  fill(255);
}

function draw() {
  background(0);
  text(lieu,50,100);
  text(degres, 50, 150);
  image(img, width/2, height/2);
}