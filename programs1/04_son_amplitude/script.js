var micro;
var fichier;
var amplitude;
var typo;

function preload() {
  fichier = loadSound('../media/fantomas.mp3')
  typo = loadFont('../media/OfficeCodePro-Medium.ttf');
}

function setup() {
  createCanvas(innerWidth/2, innerHeight);
  fill(255);
  textFont(typo);
  textAlign(CENTER);

  micro = new p5.AudioIn();
  micro.start();

  // soundFile.play();

  amplitude = new p5.Amplitude();
  amplitude.setInput(micro);
  // amplitude.smooth(0.8); // <-- try this!
}

function draw() {
  background(0);

  var level = amplitude.getLevel();
  var taille = map(level, 0, 1, 10, 200);
  textSize(taille);
  text("NOISE", width/2, height/2);


}
    