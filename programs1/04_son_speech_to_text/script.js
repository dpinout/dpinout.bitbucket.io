var enreg = new p5.SpeechRec();
var voix_sortie = new p5.Speech(); 
enreg.continuous = true;
var typo;

function preload() {
    typo= loadFont("../media/OfficeCodePro-Medium.ttf");
}

function setup() {
	createCanvas(innerWidth/2, innerHeight);
    background(0);
    textFont(typo);
    fill(255);
	textSize(32);
	textAlign(CENTER);
	text("Vas y ! Mais parle !", width / 2, height / 2);
	enreg.onResult = showResult;
	enreg.start();  
    voix_sortie.speak("say something");
}

function draw() {	
}

function showResult() {
	if (enreg.resultValue === true) {
		background(255, 0,255);
        fill(0);
		text(enreg.resultString, width / 2, height / 2);
		voix_sortie.speak(enreg.resultString); 	
	}
}
    