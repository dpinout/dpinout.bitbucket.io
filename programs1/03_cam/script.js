var video;
var taille = 20;
    
function setup() {
  createCanvas(640, 480);
  pixelDensity(1);
  video = createCapture(VIDEO);
  video.size(width/taille, height/taille);
  video.hide();
}

function draw() {
  background(255);

  video.loadPixels();
  loadPixels();
  for (var y = 0; y < video.height; y++) {
    for (var x = 0; x < video.width; x++) {
      var index = (x+y * video.width)*4;
      var r = video.pixels[index+0];
      var g = video.pixels[index+1];
      var b = video.pixels[index+2];

      var bright = (r+g+b)/3;
      var w = map(bright, 255, 0, 0, taille);
      var c = map(bright, 255, 0, 255, 0);

      noStroke();
      fill(c);
      rectMode(CENTER);
       push();
      translate(x*taille, y*taille);
      rotate(radians(c));
      rect(0,0, w, w);
      pop();
    }
  } 
}
    