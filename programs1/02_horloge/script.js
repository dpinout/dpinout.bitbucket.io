var secondes, minutes, heures;    
    
function setup(){
        createCanvas(innerWidth/2 , innerHeight);
        background(0);
        textSize(40);
        angleMode(DEGREES);
}
    
function draw() {
        background(0);
        
        secondes = second();
        minutes = minute();
        heures = hour();
        
        push();    
        fill(255, 255, 0);
        translate(width/2, height/2);        
        rotate(map(secondes, 0, 60, -90, 270));
        text(secondes, 150, 0);        
        pop();
        
        push();   
        fill(0, 255, 255);
        translate(width/2, height/2);        
        rotate(map(minutes , 0, 60, -90, 270));
        text(minutes, 80, 0);        
        pop();
        
        push();  
        fill(255, 0, 255);
        translate(width/2, height/2);        
        rotate(map(heures , 0, 60, -90, 270));
        text(heures, 10, 0);        
        pop();           
}
    