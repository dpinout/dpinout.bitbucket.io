    
var arduino = p5.board('COM5', 'arduino');
var capteur = arduino.pin(0, 'ANALOG', 'INPUT');

capteur.read(function(val){
    var lum = map(val, 900, 1020, 255, 10);
    fill(lum);
    ellipse(width/2, height/2, 50, 50);
});
    
function setup() {
    createCanvas(innerWidth/2, innerHeight);
    background(0); 
}

