    
opentype.load('../media/OfficeCodePro-Medium.ttf', function(err, font) {
    if (err) {
        alert('Font could not be loaded: ' + err);
    } else {
        // Now let's display it on a canvas with id "canvas"
        var ctx = document.getElementById('#opentype').getContext('2d');

        // Construct a Path object containing the letter shapes of the given text.
        // The other parameters are x, y and fontSize.
        // Note that y is the position of the baseline.
        var path = font.getPath('Hello, World!', 0, 150, 72);

        // If you just want to draw the text you can also use font.draw(ctx, text, x, y, fontSize).
        path.draw(ctx);
    }
});


var micro;
var fichier;
var amplitude;
var typo;

function preload() {
  fichier = loadSound('../media/fantomas.mp3')
  typo = loadFont('../media/OfficeCodePro-Medium.ttf');
}

function setup() {
  noCanvas;
  fill(255);
  textFont(typo);
  textAlign(CENTER);

  micro = new p5.AudioIn();
  micro.start();

  // soundFile.play();

  amplitude = new p5.Amplitude();
  amplitude.setInput(micro);
  // amplitude.smooth(0.8); // <-- try this!
}

function draw() {
  background(0);

  var level = amplitude.getLevel();
  var taille = map(level, 0, 1, 10, 200);
  textSize(taille);
  text("NOISE", width/2, height/2);


}
    