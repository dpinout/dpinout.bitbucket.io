var typo;
var lettre = [];
    
function preload() {
    typo = loadFont("../media/OfficeCodePro-Medium.ttf");    
}
    
function setup() {
    createCanvas(innerWidth/2, innerHeight);
    background(0);
    textFont(typo);
    textSize(40);
}
    
function draw() {
    background(0);
    if (lettre.length > 0) {   
    for (var i =0; i<lettre.length; i++){        
        lettre[i].update();
        if (lettre[i].alpha < 0) { 
            lettre.splice(i,1);
            }
        }    
    } 
}
    
    
function keyPressed() {
    lettre.push(new Lettre());
}
    
class Lettre{
    constructor() {
        this.x= int(random(width));
        this.y= int(random(height));
        this.alpha = 255;
        this.texte = `${key}`;
     
    }
    
    update() {
        fill(255, this.alpha);
        text(this.texte , this.x , this.y);
        this.y +=0.5;
        this.alpha -= 0.5;        
    } 
}