function setup() {
    loadCamera();
    loadTracker();
    loadCanvas(400,300);
}
      
function draw() {
    getPositions();    
    clear();    
    noStroke();    
    drawPoints();
}

function drawPoints() {
    for (var i=0; i<positions.length -1; i++) {
        fill(255,255,0);
        
        // draw ellipse
        noStroke();
        ellipse(positions[i][0], positions[i][1], 4, 4);
        
   
    }
}
    