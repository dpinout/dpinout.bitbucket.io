
function setup() {
    loadCamera();
    loadTracker();
    loadCanvas(400,300);
}
      
function draw() {
    getPositions();
    getEmotions();    
    clear();    
    noStroke();    
    drawPoints();

    if (emotions) {
         for (var i = 0;i < predictedEmotions.length;i++) {
    
    textSize(predictedEmotions[0].value * 60);
    text("ANGRY", 20, height-40);
    textSize(predictedEmotions[1].value * 60);
    text("SAD", 130, height-40);
    textSize(predictedEmotions[2].value * 60);
    text("SURPRISED", 220, height-40);
    textSize(predictedEmotions[3].value * 60);
    text("HAPPY", 340, height-40);
        }
        
 
    }
    
 
    
}

function drawPoints() {
    fill(255,255,0);
    for (var i=0; i<positions.length -3; i++) {
        ellipse(positions[i][0], positions[i][1], 4, 4);
    }
}
    