   

 //  Example from Jason Sigal and Golan Levin.


 var source, fft, lowPass;
 var doCenterClip = false;
 var centerClipThreshold = 0.0;
 
 var preNormalize = true;
 var postNormalize = true;
 
 
 function setup() {
   createCanvas(windowWidth/2, windowHeight);
 
   source = new p5.AudioIn();
   source.start();
 
   lowPass = new p5.LowPass();
   lowPass.disconnect();
   source.connect(lowPass);
 
   fft = new p5.FFT();
   fft.setInput(lowPass);
 }
 
 function draw() {
 
 
   var timeDomain = fft.waveform(1024, 'float32');
   var corrBuff = autoCorrelate(timeDomain); 
 
   var freq = findFrequency(corrBuff);
   var couleur = map(freq.toFixed(2),0 , 2000, 0,255);
     background(couleur);
   text ('Fundamental Frequency: ' + freq.toFixed(2), 20, 50); 
 }
 
 
 
 function autoCorrelate(timeDomainBuffer) {
   
   var nSamples = timeDomainBuffer.length;
 
 
   if (preNormalize){
     timeDomainBuffer = normalize(timeDomainBuffer);
   }
 
   if (doCenterClip) {
     timeDomainBuffer = centerClip(timeDomainBuffer);
   }
 
   var autoCorrBuffer = [];
   for (var lag = 0; lag < nSamples; lag++){
     var sum = 0; 
     for (var index = 0; index < nSamples; index++){
       var indexLagged = index+lag;
       if (indexLagged < nSamples){
         var sound1 = timeDomainBuffer[index];
         var sound2 = timeDomainBuffer[indexLagged];
         var product = sound1 * sound2;
         sum += product;
       }
     }
 
     autoCorrBuffer[lag] = sum/nSamples;
   }
 
   if (postNormalize){
     autoCorrBuffer = normalize(autoCorrBuffer);
   }
 
   return autoCorrBuffer;
 }
 
 
 
 function normalize(buffer) {
   var biggestVal = 0;
   var nSamples = buffer.length;
   for (var index = 0; index < nSamples; index++){
     if (abs(buffer[index]) > biggestVal){
       biggestVal = abs(buffer[index]);
     }
   }
   for (var index = 0; index < nSamples; index++){
 
     buffer[index] /= biggestVal;
   }
   return buffer;
 }
 
 function centerClip(buffer) {
   var nSamples = buffer.length;
 
   centerClipThreshold = map(mouseY, 0, height, 0,1); 
 
   if (centerClipThreshold > 0.0) {
     for (var i = 0; i < nSamples; i++) {
       var val = buffer[i];
       buffer[i] = (Math.abs(val) > centerClipThreshold) ? val : 0;
     }
   }
   return buffer;
 }
 
 
 function findFrequency(autocorr) {
 
   var nSamples = autocorr.length;
   var valOfLargestPeakSoFar = 0;
   var indexOfLargestPeakSoFar = -1;
 
   for (var index = 1; index < nSamples; index++){
     var valL = autocorr[index-1];
     var valC = autocorr[index];
     var valR = autocorr[index+1];
 
     var bIsPeak = ((valL < valC) && (valR < valC));
     if (bIsPeak){
       if (valC > valOfLargestPeakSoFar){
         valOfLargestPeakSoFar = valC;
         indexOfLargestPeakSoFar = index;
       }
     }
   }
   
   var distanceToNextLargestPeak = indexOfLargestPeakSoFar - 0;
 
 
   var fundamentalFrequency = sampleRate() / distanceToNextLargestPeak;
   return fundamentalFrequency;
 }
     